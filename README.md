# broken atala demo

```bash
$ npm i
$ cp .env.template .env
```

Fill in `.env` with a real value for `MEDIATOR_DID`

```bash
$ npm start

> ssb-atala-prism@0.1.0 start
> node index.js

ve [Error]: Invalid state: Unable resolve sender secret: Unable get secret: x25519: seed must be 32 bytes
    at Oi.achieveMediation (/home/me/broken-atala-demo/node_modules/@atala/prism-wallet-sdk/build/node/index.js:1:152250)
    at async mi.registerMediator (/home/me/broken-atala-demo/node_modules/@atala/prism-wallet-sdk/build/node/index.js:1:143371)
    at async Ai.start (/home/me/broken-atala-demo/node_modules/@atala/prism-wallet-sdk/build/node/index.js:1:154237)
```

