const SDK = require('@atala/prism-wallet-sdk')
require('dotenv').config()

const Pluto = require('./pluto')

function createAgent (mediatorDIDString) {
  const mediatorDID = SDK.Domain.DID.fromString(mediatorDIDString)

  const apollo = new SDK.Apollo()
  const api = new SDK.ApiImpl()
  const castor = new SDK.Castor(apollo)
  const pluto = new Pluto()

  const didcomm = new SDK.DIDCommWrapper(apollo, castor, pluto)
  const mercury = new SDK.Mercury(castor, didcomm, api)
  const store = new SDK.PublicMediatorStore(pluto)
  const handler = new SDK.BasicMediatorHandler(mediatorDID, mercury, store)
  const manager = new SDK.ConnectionsManager(castor, mercury, pluto, handler)

  const seed = apollo.createRandomSeed()

  return new SDK.Agent(
    apollo,
    castor,
    pluto,
    mercury,
    handler,
    manager,
    seed.seed
  )
}

const agent = createAgent(process.env.MEDIATOR_DID)

agent.start()
  .then(() => console.log('NICE!'))
  .catch(err => console.error(err))
